--Insert Records Script

INSERT INTO booking_details VALUES (1,12345,'Signature Hotel','2020-07-17 10:10:10','2020-07-20 07:00:00','Lewis Hamilton','lewis@gmail.com','123456789',113,'Single Room',1,210);
INSERT INTO booking_details VALUES (2,46578,'Sunway Velocity Hotel','2020-07-15 08:20:35','2020-07-22 07:00:00','Max Verstappen','max@gmail.com','987654321',718,'Double Room',2,700);
INSERT INTO booking_details VALUES (3,98765,'Grand Hyatt Hotel','2020-06-10 12:45:17','2020-06-20 07:00:00','Charles Leclerc','charles@gmail.com','123456789',502,'King Room',2,1020);
INSERT INTO booking_details VALUES (4,14785,'The Majestic Hotel','2020-07-01 12:09:10','2020-07-05 07:00:00','Carlos Sainz','carlos@gmail.com','123456789',703,'Double Room',2,660);
INSERT INTO booking_details VALUES (5,36985,'Hilton Hotel','2020-07-09 08:33:00','2020-07-13 07:00:00','Valtteri Bottas','bottas@gmail.com','123456789',415,'King Room',2,580);