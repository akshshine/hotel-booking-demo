Intro
======
User hotel Booking Demo project.

Configuration For Running project on local machine
===================================================

Database used : MySQL
Schema Name: hotel_booking
Database username: root
Database password: root
Database port: 3306

Change the username,password and port for MySQL as per your local MySQL configuration to make run application successfully.

Execute create_db.sql file from database folder under resources to create table.
Also execute insert_sample_records.sql from same folder to create some initial records if wanted.

Steps to Run Project:
=====================
1. Import project into Eclipse/STS or Any other IDE.
2. Build the application to download all required dependencies
3. After build successfully Run the application.
4. Once server started visit following URL http://localhost:8080swagger-ui.html in browser
5. Application port is 8080.

Test Scenarios
==============
1) To display all users use following API
/hotelBooking/getAll
it will display all records from table like as following (only If the insert_sample_record.sql is executed or any other record inserted)
[
    {
        "bookingId": 1,
        "hotelId": 12345,
        "hotelName": "Signature Hotel",
        "checkInDate": "2020-07-17T10:10:10.000+0000",
        "checkOutDate": "2020-07-20T07:00:00.000+0000",
        "custName": "Lewis Hamilton",
        "custEmail": "lewis@gmail.com",
        "custContact": "123456789",
        "roomId": 113,
        "roomName": "Single Room",
        "guestCount": 1,
        "totalAmount": 210
    },
    {
        "bookingId": 2,
        "hotelId": 46578,
        "hotelName": "Sunway Velocity Hotel",
        "checkInDate": "2020-07-15T08:20:35.000+0000",
        "checkOutDate": "2020-07-22T07:00:00.000+0000",
        "custName": "Max Verstappen",
        "custEmail": "max@gmail.com",
        "custContact": "987654321",
        "roomId": 718,
        "roomName": "Double Room",
        "guestCount": 2,
        "totalAmount": 700
    },
    {
        "bookingId": 3,
        "hotelId": 98765,
        "hotelName": "Grand Hyatt Hotel",
        "checkInDate": "2020-06-10T12:45:17.000+0000",
        "checkOutDate": "2020-06-20T07:00:00.000+0000",
        "custName": "Charles Leclerc",
        "custEmail": "charles@gmail.com",
        "custContact": "123456789",
        "roomId": 502,
        "roomName": "King Room",
        "guestCount": 2,
        "totalAmount": 1020
    },
    {
        "bookingId": 4,
        "hotelId": 14785,
        "hotelName": "The Majestic Hotel",
        "checkInDate": "2020-07-01T12:09:10.000+0000",
        "checkOutDate": "2020-07-05T07:00:00.000+0000",
        "custName": "Carlos Sainz",
        "custEmail": "carlos@gmail.com",
        "custContact": "123456789",
        "roomId": 703,
        "roomName": "Double Room",
        "guestCount": 2,
        "totalAmount": 660
    },
    {
        "bookingId": 5,
        "hotelId": 36985,
        "hotelName": "Hilton Hotel",
        "checkInDate": "2020-07-09T08:33:00.000+0000",
        "checkOutDate": "2020-07-13T07:00:00.000+0000",
        "custName": "Valtteri Bottas",
        "custEmail": "bottas@gmail.com",
        "custContact": "123456789",
        "roomId": 415,
        "roomName": "King Room",
        "guestCount": 2,
        "totalAmount": 580
    },
    {
        "bookingId": 6,
        "hotelId": 78451,
        "hotelName": "JW Marriott Hotel",
        "checkInDate": "2020-07-20T05:30:10.000+0000",
        "checkOutDate": "2020-07-25T07:00:00.000+0000",
        "custName": "John Cena",
        "custEmail": "cena@gmail.com",
        "custContact": "147258369",
        "roomId": 721,
        "roomName": "King Room",
        "guestCount": 2,
        "totalAmount": 850
    }
]

2)To get particular record by id
use following API
/hotelBooking/getById/4

you will get result record having bookingId = 4
{
    "bookingId": 4,
    "hotelId": 14785,
    "hotelName": "The Majestic Hotel",
    "checkInDate": "2020-07-01T12:09:10.000+0000",
    "checkOutDate": "2020-07-05T07:00:00.000+0000",
    "custName": "Carlos Sainz",
    "custEmail": "carlos@gmail.com",
    "custContact": "123456789",
    "roomId": 703,
    "roomName": "Double Room",
    "guestCount": 2,
    "totalAmount": 660
}


3) To Create Booking record
use following API
hotelBooking/createBooking 
with following JSON
a)
{
	"hotelId": 78451,
	"hotelName": "JW Marriott Hotel",
	"checkInDate": "2020-07-14T05:30:10.000+00:00",
	"checkOutDate": "2020-07-17T07:00:00.000+00:00",
	"custName": "Sachin tendulkar",
	"custEmail": "sachin@gmail.com",
	"custContact": "654987321",
	"roomId": 435,
	"roomName": "King Room",
	"guestCount": 2,
	"totalAmount": 850
}

record will be inserted into DB.

b) 
{
     "hotelId": 98765,
     "hotelName": "Grand Hyatt Hotel",
     "checkInDate": "2020-06-10T12:45:17.000+0000",
     "checkOutDate": "2020-06-20T07:00:00.000+0000",
     "custName": "Charles Leclerc",
     "custEmail": "charles@gmail.com",
     "custContact": "123456789",
     "roomId": 502,
     "roomName": "King Room",
     "guestCount": 2,
     "totalAmount": 1020
}

record will be inserted into DB.

Testing on AWS
==============
Use following URL to test same application on AWS
http://hotelbooking-env.eba-cq97rr6y.us-east-2.elasticbeanstalk.com/swagger-ui.html

Test Scenario's will be same.