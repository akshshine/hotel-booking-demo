package com.demo.hotelbooking.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.hotelbooking.model.HotelBooking;
import com.demo.hotelbooking.repo.HotelBookingRepository;
import com.demo.hotelbooking.service.HotelBookingService;

@Service
public class HotelBookingServiceImpl implements HotelBookingService {

	@Autowired
	HotelBookingRepository hotelBookingRepository;

	@Override
	public List<HotelBooking> getAllBookings() {
		return (List<HotelBooking>) hotelBookingRepository.findAll();
	}

	@Override
	public HotelBooking getOneById(Long hotelId) {
		return hotelBookingRepository.findById(hotelId).get();
	}

	@Override
	public HotelBooking createBooking(HotelBooking hotelBooking) {
		return hotelBookingRepository.save(hotelBooking);
	}

}
